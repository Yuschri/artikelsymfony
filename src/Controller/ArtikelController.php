<?php

namespace App\Controller;

use App\Entity\Artikel;
use App\Entity\Penulis;
use App\Form\ArtikelType;
use App\Repository\ArtikelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @Route("/artikel")
 */
class ArtikelController extends AbstractController
{
    /**
     * @Route("/", name="artikel_index", methods="GET")
     */
    public function index(ArtikelRepository $artikelRepository): Response
    {
        return $this->render('artikel/index.html.twig', ['artikels' => $artikelRepository->findAll()]);
    }

    /**
     * @Route("/new", name="artikel_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $repository = $this->getDoctrine()->getRepository(Penulis::class);
        $penulis = $repository->findAll();

        return $this->render('artikel/new.html.twig', [
            'penulis' => $penulis,
        ]);
    }

    /**
     * @Route("/{id}", name="artikel_show", methods="GET")
     */
    public function show(Artikel $artikel, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $art = $em->getRepository(Artikel::class)
            ->find($id);
        $art->setCounter(($art->getCounter()+1));
        $em->flush();
        return $this->render('artikel/show.html.twig', ['artikel' => $artikel]);
    }

    /**
     * @Route("/{id}/edit", name="artikel_edit", methods="GET|POST")
     */
    public function edit(Request $request, $id): Response
    {
        $repositoryp = $this->getDoctrine()->getRepository(Penulis::class);
        $repositorya = $this->getDoctrine()->getRepository(Artikel::class);
        $artikel = $repositorya->find($id);
        $penulis = $repositoryp->findAll();

        $form = $this->createForm(ArtikelType::class, $artikel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('artikel_edit', ['id' => $artikel->getId()]);
        }

        return $this->render('artikel/edit.html.twig', [
            'artikel' => $artikel,
            'penulis' => $penulis,
        ]);
    }

    /**
     * @Route("/{id}", name="artikel_delete", methods="DELETE")
     */
    public function delete(Request $request, Artikel $artikel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$artikel->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository(Artikel::class);
            $data = $repository->find($artikel->getId());
            if(!unlink($data->getFoto())){
                echo "yeyek";
                die();
            }
            $em->remove($artikel);
            $em->flush();
        }

        return $this->redirectToRoute('artikel_index');
    }

    /**
     * @Route("/create", name="artikel_create", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create(Request $request)
    {
        $data = $request->request->all();
        $data['foto'] = $request->files->get('foto');

        $errors = Validation::createValidator()->validate($data, new Assert\Collection([
            'judul' => new Assert\NotBlank(['message' => 'Nama barang tidak boleh kosong']),
            'isi' => new Assert\NotBlank(['message' => 'Pilih Prodi']),
            'jenis' => new Assert\NotBlank(['message' => 'Alamat tidak boleh kosong']),
            'penulis' => new Assert\NotBlank(['message' => 'NIM tidak boleh kosong']),
            'lolos_edit' => new Assert\NotBlank(['message' => 'Pilih Jenis Kelamin']),
            'foto' => new Assert\NotBlank(['message' => 'Foto tidak boleh kosong']),
            'foto' => new Assert\Image()
        ]));

        if ($errors->count()>0)
        {
            var_dump($data);
            foreach ($errors as $error){
                print $error->getMessage()."<br>";
            }
            die();
        }
        else
        {
            $cek_artikel = $this->getDoctrine()
                ->getRepository(Artikel::class)
                ->findBy(['judul' => $request->get('judul')]);

            if (empty($cek_artikel)){
                $em = $this->getDoctrine()->getManager();

                $artikel = new Artikel();

                if (!empty($data['foto'])){
                    if (file_exists($path = '/images/' . $artikel->getFoto())) {
                        unlink($path);
                    }

                    $foto = $request->files->get('foto');
                    $foto->move('images', $foto->getClientOriginalName());

                    $artikel->setFoto('images/'.$foto->getClientOriginalName());
                }

                $penulis = $this->getDoctrine()
                    ->getRepository(Penulis::class)
                    ->find($request->get('penulis'));


                $artikel->setJudul($request->get('judul'));
                $artikel->setIdPenulis($penulis);
                $artikel->setIsi($request->get('isi'));
                $artikel->setCounter(0);
                $artikel->setJenis($request->get('jenis'));
                $artikel->setLolosEdit($request->get('lolos_edit'));

                $em->persist($artikel);
                $em->flush();

                $this->addFlash('success', "Berhasil menambahkan Artikel {$request->get('judul')}");

                return $this->redirectToRoute('artikel_index');
            }
            $this->addFlash('error',"Artikel yang berjudul {$request->get('judul')} telah ada");
        }
        return $this->redirectToRoute('artikel_index');
    }

    /**
     * @Route("/artikel/update", name="artikel_update", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request){

        $em = $this->getDoctrine()->getManager();

        $artikel = $em->getRepository(Artikel::class)
            ->find($request->get('id'));

        if (!$artikel){
            $this->addFlash('error', "Artikel yang berjudul {$request->get('judul')} tidak ditemukan");

            return $this->redirectToRoute('artikel');
        }

        if (!empty($data['foto'])){
            if (file_exists($path = '/foto/' . $artikel->getFoto())) {
                unlink($path);
            }

            $foto = $request->files->get('foto');
            $foto->move('foto', $foto->getClientOriginalName());

            $artikel->setFoto($foto->getClientOriginalName());
        }

        $penulis = $this->getDoctrine()
            ->getRepository(Penulis::class)
            ->find($request->get('penulis_id'));


        $artikel->setJudul($request->get('judul'));
        $artikel->setPenulisId($penulis);
        $artikel->setJenis($request->get('jenis'));
        $artikel->setIsi($request->get('isi'));
        $artikel->setLolosEdit($request->get('lolos_edit'));

        $em->flush();

        $this->addFlash('success', "Berhasil memperbarui data artikel");

        return $this->redirectToRoute('artikel');
    }
}
