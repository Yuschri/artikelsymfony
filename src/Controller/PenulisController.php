<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Penulis;
use Knp\Component\Pager\Pagination;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class PenulisController extends AbstractController
{
    /**
     * @Route("/penulis", name="penulis")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $enmana = $this->getDoctrine()->getManager();

        $repository = $enmana->getRepository(Penulis::class);
        $penulis = $repository->createQueryBuilder('p')->getQuery();

        $perPage = 5;

        $data = $paginator->paginate(
            $penulis,
            $request->query
                ->getInt('page', 1),
            $perPage
        );

        $currentPage = (int)$request->get('page');
        $currentPage = ($currentPage == 0) ? 1 : $currentPage;

        return $this->render('penulis/index.html.twig', [
            'penulis' => $data,
            'currentPage' => $currentPage,
            'perPage' => 5,
            'controller_name' => 'PenulisController',
        ]);
    }
}
