<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\PenulisController;

class DefaultController extends AbstractController
{
     /**
      * @Route("/")
      */
    public function index()
    {
        return $this->redirectToRoute('artikel_index');
    }
}