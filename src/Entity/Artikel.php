<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtikelRepository")
 */
class Artikel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $judul;

    /**
     * @ORM\Column(type="text")
     */
    private $isi;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $foto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jenis;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lolos_edit;

        /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Penulis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $penulis;

    /**
     * @ORM\Column(type="integer")
     */
    private $counter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJudul(): ?string
    {
        return $this->judul;
    }

    public function setJudul(string $judul): self
    {
        $this->judul = $judul;

        return $this;
    }

    public function getIsi(): ?string
    {
        return $this->isi;
    }

    public function setIsi(string $isi): self
    {
        $this->isi = $isi;

        return $this;
    }

    public function getFoto(): ?string
    {
        return $this->foto;
    }

    public function setFoto(string $foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getJenis(): ?string
    {
        return $this->jenis;
    }

    public function setJenis(string $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    public function getLolosEdit(): ?string
    {
        return $this->lolos_edit;
    }

    public function setLolosEdit(string $lolos_edit): self
    {
        $this->lolos_edit = $lolos_edit;

        return $this;
    }

    public function getCounter(): ?int
    {
        return $this->counter;
    }

    public function setCounter(int $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    public function getIdPenulis(): ?Penulis
    {
        return $this->penulis;
    }

    public function setIdPenulis(?Penulis $penulis): self
    {
        $this->penulis = $penulis;

        return $this;
    }
}
