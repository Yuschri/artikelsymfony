<?php

namespace App\Repository;

use App\Entity\Penulis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Penulis|null find($id, $lockMode = null, $lockVersion = null)
 * @method Penulis|null findOneBy(array $criteria, array $orderBy = null)
 * @method Penulis[]    findAll()
 * @method Penulis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PenulisRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Penulis::class);
    }

//    /**
//     * @return Penulis[] Returns an array of Penulis objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Penulis
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
